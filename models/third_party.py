from sqlalchemy import Column, Integer, Text
from sqlalchemy.orm import relationship

from .database import Base


# We will use this Base class we created before to create the SQLAlchemy models
class ThirdParty(Base):
    __tablename__ = "third_parties"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(Text)

    transactions = relationship("Transaction", back_populates="third_party")
