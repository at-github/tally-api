from sqlalchemy import Column, Integer, Date, ForeignKey
from sqlalchemy.orm import relationship

from .database import Base


# We will use this Base class we created before to create the SQLAlchemy models
class Transaction(Base):
    __tablename__ = "transactions"

    id = Column(Integer, primary_key=True, index=True)
    amount = Column(Integer)
    date = Column(Date)

    third_party_id = Column(
        Integer,
        ForeignKey(
            "third_parties.id",
            ondelete='SET NULL'
        ),
        nullable=True
    )
    third_party = relationship("ThirdParty", back_populates="transactions")
