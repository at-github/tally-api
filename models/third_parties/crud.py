from sqlalchemy.orm import Session

import models.third_party as models
from schemas.third_party import ThirdPartyCreate, ThirdParty


def get_third_parties(db: Session):
    return db.query(models.ThirdParty).all()


def get_third_party(db: Session, third_party_id: int):
    return db.query(models.ThirdParty).filter(
        models.ThirdParty.id == third_party_id
    ).first()


def create_third_party(db: Session, third_party: ThirdPartyCreate):
    db_third_party = models.ThirdParty(
        name=third_party.name
    )
    db.add(db_third_party)
    db.commit()
    db.refresh(db_third_party)

    return db_third_party


def update_third_party(
    db: Session,
    third_party_id: int,
    third_party: ThirdParty
):
    db.query(models.ThirdParty).filter(
        models.ThirdParty.id == third_party_id
    ).update(third_party.__dict__)
    db.commit()

    return get_third_party(db, third_party_id)


def delete_third_party(db: Session, third_party_id: int):
    db.query(models.ThirdParty).filter(
        models.ThirdParty.id == third_party_id
    ).delete()
    db.commit()
