from fastapi import FastAPI, status
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware

from routers import third_parties, transactions


def init_app():
    app = FastAPI()

    app.include_router(third_parties.router)
    app.include_router(transactions.router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            'http://localhost:3000',
        ],
        allow_methods=['GET', 'POST', 'DELETE', 'PUT']
    )

    @app.get(
        '/favicon.ico',
        include_in_schema=False,
        status_code=status.HTTP_200_OK
    )
    def favicon():
        return FileResponse('static/favicon.ico')

    return app
