[![pipeline status](https://gitlab.com/tarikamar/tally-api/badges/master/pipeline.svg)](https://gitlab.com/tarikamar/tally-api/-/commits/master) [![coverage report](https://gitlab.com/tarikamar/tally-api/badges/master/coverage.svg)](https://gitlab.com/tarikamar/tally-api/-/commits/master)

# Accounting tally app

## Getting started

### Configure the application

```shell
ENV=[prod|dev|test]
DEBUG=[True|False]
DB_HOST=db_host
DB_NAME='db name'
DB_USER='db user'
DB_PASSWORD='db password'
```

### Install dependencies

```shell
pip install -r requirements.txt
```

```shell
# Update database
ENV=prod alembic upgrade head
```

### Launch the app

```shell
uvicorn app:app --host hostdomain --port xxxx --env-file .env
```
#### For dev

```shell
# Update database for dev
alembic upgrade head
```

```shell
# Or with live reload & env = development
# By default, env = dev
uvicorn main:init --host hostdomain --port xxxx --reload
```

## Launch tests

```shell
# Update database for test
ENV=test alembic upgrade head
```

```shell
pytest

# Or
ENV=test pytest -f --color yes

# Or watching
ENV=test ptw
```

### Or see coverage
```shell
pytest --cov=. tests
```

## Migration

```shell
#Create migration

alembic revision --autogenerate -m "Initial migration"
```

```shell
#Apply last migration

alembic upgrade head
```

```shell
#List migrations

alembic history
```

```shell
#Show specific migration

alembic show ref
```

```shell
#Restore specific migration

alembic downgrade ref
```

## CI

Launch the job named `test`
```shell
gitlab-runner exec docker test
```
