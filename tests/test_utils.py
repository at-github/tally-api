import unittest.mock
from unittest.mock import MagicMock
from utils import get_env_extension


@unittest.mock.patch('os.environ')
def test_get_env_extension_for_test(os_environ):
    os_environ.get = MagicMock(return_value='test')

    assert get_env_extension() == '.env.test'
    os_environ.get.assert_called_once_with('ENV')


@unittest.mock.patch('os.environ')
def test_get_env_extension_for_prod(os_environ):
    os_environ.get = MagicMock(return_value='prod')

    assert get_env_extension() == '.env'
    os_environ.get.assert_called_once_with('ENV')


@unittest.mock.patch('os.environ')
def test_get_env_extension_for_unknown_env(os_environ):
    os_environ.get = MagicMock(return_value='')

    assert get_env_extension() == '.env.dev'
    os_environ.get.assert_called_once_with('ENV')
