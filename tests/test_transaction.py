from sqlalchemy.orm import Session
from fastapi.testclient import TestClient
import pytest

import models.transaction as models
from models.database import SessionLocal

from .utils import add_third_party, add_transaction

from app import init_app

app = init_app()

client = TestClient(app)


# GET /transactions
def test_list_transactions(db_session: Session):
    # Context
    db_third_party1 = add_third_party(db_session, name="One")
    db_third_party2 = add_third_party(db_session, name="Two")

    db_transaction1 = add_transaction(
        db_session, third_party_id=db_third_party1.id
    )
    db_transaction2 = add_transaction(
        db_session, third_party_id=db_third_party1.id
    )
    db_transaction3 = add_transaction(
        db_session, third_party_id=db_third_party2.id
    )

    # Action
    response = client.get('/transactions')

    # Assertions
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": db_transaction1.id,
            "amount": db_transaction1.amount,
            "date": db_transaction1.date.isoformat(),
            "third_party": {
                "id": db_third_party1.id,
                "name": db_third_party1.name,
            }
        }, {
            "id": db_transaction2.id,
            "amount": db_transaction2.amount,
            "date": db_transaction2.date.isoformat(),
            "third_party": {
                "id": db_third_party1.id,
                "name": db_third_party1.name,
            }
        }, {
            "id": db_transaction3.id,
            "amount": db_transaction3.amount,
            "date": db_transaction3.date.isoformat(),
            "third_party": {
                "id": db_third_party2.id,
                "name": db_third_party2.name,
            }
        }
    ]


def test_list_transactions_order_by_date_asc(db_session: Session):
    # Context
    db_transaction1 = add_transaction(db_session, date="2023-11-30")
    db_transaction2 = add_transaction(db_session, date="2024-11-29")

    # Action
    response = client.get('/transactions?sort=date&order=asc')

    # Assertions
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": db_transaction1.id,
            "amount": db_transaction1.amount,
            "date": db_transaction1.date.isoformat(),
            "third_party": None
        }, {
            "id": db_transaction2.id,
            "amount": db_transaction2.amount,
            "date": db_transaction2.date.isoformat(),
            "third_party": None
        }
    ]


def test_list_transactions_order_by_amount_desc(db_session: Session):
    # Context
    db_transaction1 = add_transaction(
        db_session,
        amount="203",
        date="2023-11-05"
    )
    db_transaction2 = add_transaction(
        db_session,
        amount="204",
        date="2023-11-04"
    )
    db_transaction3 = add_transaction(
        db_session,
        amount="205",
        date="2023-11-03"
    )

    # Action
    response = client.get('/transactions?sort=amount&order=desc')

    # Assertions
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": db_transaction3.id,
            "amount": db_transaction3.amount,
            "date": db_transaction3.date.isoformat(),
            "third_party": None
        }, {
            "id": db_transaction2.id,
            "amount": db_transaction2.amount,
            "date": db_transaction2.date.isoformat(),
            "third_party": None
        }, {
            "id": db_transaction1.id,
            "amount": db_transaction1.amount,
            "date": db_transaction1.date.isoformat(),
            "third_party": None
        }
    ]


def test_list_empty_transaction():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.get('/transactions')

    # Assertions
    assert response.status_code == 200
    assert response.json() == []


# GET /transactions/{id}
def test_read_existing_transaction_without_third_party(db_session: Session):
    # Context
    amount = 700
    date = "2023-11-29"
    db_transaction = add_transaction(db_session, amount=amount, date=date)

    # Action
    response = client.get('/transactions/{id}'.format(id=db_transaction.id))

    # Assertions
    assert response.status_code == 200
    assert response.json() == {
        "id": db_transaction.id,
        "amount": amount,
        "date": date,
        "third_party": None
    }


def test_read_complete_transaction(db_session: Session):
    # Context
    amount = 700
    date = "2023-11-29"
    db_third_party = add_third_party(db_session, name='3rd party')
    db_transaction = add_transaction(
        db_session,
        amount=amount,
        date=date,
        third_party_id=db_third_party.id
    )

    # Action
    response = client.get('/transactions/{id}'.format(id=db_transaction.id))

    # Assertions
    assert response.status_code == 200
    assert response.json() == {
        "id": db_transaction.id,
        "amount": amount,
        "date": date,
        "third_party": {
            "id": db_third_party.id,
            "name": db_third_party.name
        }
    }


def test_read_unexisting_transaction():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.get('/transactions/{id}'.format(id=0))

    # Assertions
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Transaction not found"
    }


# POST /transactions
def test_create_complete_transaction(db_session: Session):
    # Context
    # Table transactions is cleared before test, so the context is empty data

    db_third_party = add_third_party(
        db_session,
        'Third party for transaction test'
    )
    amount = 1000
    date = "2023-11-30"

    # Action
    response = client.post(
        '/transactions',
        json={
            "amount": amount,
            "date": date,
            "third_party_id": db_third_party.id
        }
    )
    assert response.status_code == 201
    assert response.json().items() >= {
        "amount": amount,
        "date": date,
        "third_party": {
            "id": db_third_party.id,
            "name": db_third_party.name
        }
    }.items()


def test_create_transaction_without_amount():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.post(
        '/transactions',
        json={"date": "2023-11-30"}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "amount"]
    }.items()


def test_create_transaction_without_date():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.post(
        '/transactions',
        json={"amount": 5000}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "date"]
    }.items()


def test_create_transaction_without_third_party():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.post(
        '/transactions',
        json={"amount": 5000, "date": "2023-02-01"}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "third_party_id"]
    }.items()


# PUT /transactions
def test_edit_transaction(db_session: Session):
    # Context

    db_third_party_before = add_third_party(db_session, name="Third party old")
    amount = 1100
    date = "2023-11-30"
    db_transaction = add_transaction(
        db_session,
        amount=amount,
        date=date,
        third_party_id=db_third_party_before.id
    )

    new_amount = amount + 1000
    new_date = "2024-12-03"
    db_third_party_after = add_third_party(db_session, name="Third party new")

    # Action
    response = client.put(
        '/transactions/{id}'.format(id=db_transaction.id),
        json={
            "amount": new_amount,
            "date": new_date,
            "third_party_id": db_third_party_after.id
        }
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": db_transaction.id,
        "amount": new_amount,
        "date": new_date,
        "third_party": {
            "id": db_third_party_after.id,
            "name": db_third_party_after.name
        }
    }


def test_edit_transaction_without_amount(db_session: Session):
    # Context
    db_transaction = add_transaction(
        db_session,
        amount=1200,
        date="2023-11-30"
    )

    # Action
    response = client.put(
        '/transactions/{id}'.format(id=db_transaction.id),
        json={"date": "2023-11-30"}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "amount"]
    }.items()


def test_edit_transaction_without_date(db_session: Session):
    # Context
    db_transaction = add_transaction(
        db_session,
        amount=1200,
        date="2023-11-30"
    )

    # Action
    response = client.put(
        '/transactions/{id}'.format(id=db_transaction.id),
        json={"amount": 5000}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "date"]
    }.items()


def test_edit_transaction_without_third_party(db_session: Session):
    # Context
    db_transaction = add_transaction(
        db_session,
        amount=1200,
        date="2023-11-30"
    )

    # Action
    response = client.put(
        '/transactions/{id}'.format(id=db_transaction.id),
        json={"amount": 5000, "date": "2024-02-01"}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "third_party_id"]
    }.items()


def test_update_unexisting_transaction():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.put(
        '/transactions/{id}'.format(id=0),
        json={"amount": 6000, "date": "2023-11-30", "third_party_id": 0}
    )

    # Assertions
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Transaction not found"
    }


# DELETE /transactions
def test_delete_transaction(db_session: Session):
    # Context
    db_transaction = add_transaction(
        db_session,
        amount=1200,
        date="2023-11-30"
    )

    # Action
    response = client.delete('/transactions/{id}'.format(id=db_transaction.id))
    db_transactions = db_session.query(models.Transaction).all()

    # Assertions
    assert response.status_code == 204
    assert not db_transactions


def test_delete_unexisting_transaction(db_session):
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.delete('/transactions/{id}'.format(id=0))

    # Assertions
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Transaction not found"
    }


@pytest.fixture(autouse=True)
def db_session():
    db_session = SessionLocal()
    db_session.query(models.Transaction).delete()
    db_session.commit()
    yield db_session
    db_session.close()
