from sqlalchemy.orm import Session
import models.third_party as third_party_model
import models.transaction as transaction_model


def add_third_party(
    session: Session,
    name: str = 'Third party test'
):
    db_third_party = third_party_model.ThirdParty(name=name)
    session.add(db_third_party)
    session.commit()
    session.refresh(db_third_party)

    return db_third_party


def add_transaction(
    session: Session,
    amount: int = 100,
    date: str = "2023-11-30",
    third_party_id: int = None
):
    db_transaction = transaction_model.Transaction(
        amount=amount,
        date=date,
        third_party_id=third_party_id
    )
    session.add(db_transaction)
    session.commit()
    session.refresh(db_transaction)

    return db_transaction
