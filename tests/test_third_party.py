from sqlalchemy.orm import Session
from fastapi.testclient import TestClient
import pytest

import models.transaction as transaction_model
import models.third_party as models
from models.database import SessionLocal

from .utils import add_third_party, add_transaction

from app import init_app


app = init_app()

client = TestClient(app)

SLUG = '/third-parties'


# GET /third-parties
def test_list_third_parties(db_session: Session):
    # Context
    db_third_party1 = add_third_party(db_session, name="Third party One")
    db_third_party2 = add_third_party(db_session, name="3rd party 2")

    db_transaction1 = add_transaction(
        db_session,
        amount=1,
        date="2023-01-31",
        third_party_id=db_third_party1.id
    )
    db_transaction2 = add_transaction(
        db_session,
        amount=2,
        date="2023-01-31",
        third_party_id=db_third_party1.id
    )
    db_transaction3 = add_transaction(
        db_session,
        amount=3,
        date="2023-01-31",
        third_party_id=db_third_party2.id
    )

    # Action
    response = client.get(SLUG)

    # Assertions
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": db_third_party1.id,
            "name": db_third_party1.name,
            "transactions": [
                {
                    "id": db_transaction1.id,
                    "amount": db_transaction1.amount,
                    "date": db_transaction1.date.isoformat(),
                },
                {
                    "id": db_transaction2.id,
                    "amount": db_transaction2.amount,
                    "date": db_transaction2.date.isoformat(),
                }
            ]
        }, {
            "id": db_third_party2.id,
            "name": db_third_party2.name,
            "transactions": [
                {
                    "id": db_transaction3.id,
                    "amount": db_transaction3.amount,
                    "date": db_transaction3.date.isoformat(),
                }
            ]
        }
    ]


def test_list_empty_third_parties():
    # Context
    # Table third_parties is cleared before test, so the context is empty data

    # Action
    response = client.get(SLUG)

    # Assertions
    assert response.status_code == 200
    assert response.json() == []


# GET /third-parties/{id}
def test_read_existing_third_party_without_third_party(db_session: Session):
    # Context
    name = "Existing third party test name"
    db_third_party = add_third_party(db_session, name=name)

    # Action
    response = client.get(
        '{slug}/{id}'.format(slug=SLUG, id=db_third_party.id)
    )

    # Assertions
    assert response.status_code == 200
    assert response.json() == {
        "id": db_third_party.id,
        "name": name,
        "transactions": []
    }


def test_read_complete_third_party(db_session: Session):
    # Context
    name = "Existing third party test name"
    db_third_party = add_third_party(db_session, name=name)
    db_transaction = add_transaction(
        db_session,
        amount="203",
        date="2023-11-05",
        third_party_id=db_third_party.id
    )

    # Action
    response = client.get(
        '{slug}/{id}'.format(slug=SLUG, id=db_third_party.id)
    )

    # Assertions
    assert response.status_code == 200
    assert response.json() == {
        "id": db_third_party.id,
        "name": name,
        "transactions": [
            {
                "id": db_transaction.id,
                "amount": db_transaction.amount,
                "date": db_transaction.date.isoformat()
            }
        ]
    }


def test_read_unexisting_third_party():
    # Context
    # Table third_parties is cleared before test, so the context is empty data

    # Action
    response = client.get('{slug}/{id}'.format(slug=SLUG, id=0))

    # Assertions
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Third party not found"
    }


# POST /third-parties
def test_create_third_party():
    # Context
    # Table third_parties is cleared before test, so the context is empty data

    name = "TP name"

    # Action
    response = client.post(
        SLUG,
        json={
            "name": name
        }
    )
    assert response.status_code == 201
    assert response.json().items() >= {
        "name": name
    }.items()


def test_create_third_party_without_name():
    # Context
    # Table third_parties is cleared before test, so the context is empty data

    # Action
    response = client.post(
        SLUG,
        json={}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "name"]
    }.items()


# PUT /third-parties
def test_edit_third_party(db_session: Session):
    # Context

    name = "Third party test"
    db_third_party = add_third_party(db_session, name=name)

    new_name = "Edited third party name test"
    # Action
    response = client.put(
        '{slug}/{id}'.format(slug=SLUG, id=db_third_party.id),
        json={
            "name": new_name
        }
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": db_third_party.id,
        "name": new_name
    }


def test_edit_third_party_without_name(db_session: Session):
    # Context
    db_third_party = add_third_party(
        db_session,
        name="Name of the third party"
    )

    # Action
    response = client.put(
        '{slug}/{id}'.format(slug=SLUG, id=db_third_party.id),
        json={}
    )

    detail = response.json()["detail"][0]
    assert response.status_code == 422
    assert detail.items() >= {
        "msg": "Field required",
        "type": "missing",
        "loc": ["body", "name"]
    }.items()


def test_update_unexisting_transaction():
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.put(
        '{slug}/{id}'.format(slug=SLUG, id=0),
        json={"name": "name ?"}
    )

    # Assertions
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Third party not found"
    }


# DELETE /third-parties
def test_delete_third_party(db_session: Session):
    # Context
    db_third_party = add_third_party(
        db_session,
        name="Died third party"
    )

    # Action
    response = client.delete(
        '{slug}/{id}'.format(slug=SLUG, id=db_third_party.id)
    )
    db_third_parties = db_session.query(models.ThirdParty).all()

    # Assertions
    assert response.status_code == 204
    assert not db_third_parties


def test_delete_third_party_with_transactions(db_session: Session):
    # Context
    db_third_party = add_third_party(
        db_session,
        name="Died third party"
    )
    add_transaction(
        db_session,
        amount=1,
        date="2023-02-05",
        third_party_id=db_third_party.id
    )
    add_transaction(
        db_session,
        amount=1,
        date="2023-02-05",
        third_party_id=db_third_party.id
    )

    # Action
    response = client.delete(
        '{slug}/{id}'.format(slug=SLUG, id=db_third_party.id)
    )
    db_third_parties = db_session.query(models.ThirdParty).all()

    # Assertions third party
    assert response.status_code == 204
    assert not db_third_parties

    # Assertions transactions
    transactions = db_session.query(transaction_model.Transaction).all()
    assert len(transactions) == 2
    for transaction in transactions:
        assert transaction.third_party_id is None


def test_delete_unexisting_third_party(db_session):
    # Context
    # Table transactions is cleared before test, so the context is empty data

    # Action
    response = client.delete('{slug}/{id}'.format(slug=SLUG, id=0))

    # Assertions
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Third party not found"
    }


@pytest.fixture(autouse=True)
def db_session():
    db_session = SessionLocal()
    db_session.query(transaction_model.Transaction).delete()
    db_session.query(models.ThirdParty).delete()
    db_session.commit()
    yield db_session
    db_session.close()
