from sqlalchemy.orm import declarative_base
from models.database import engine

from app import init_app

Base = declarative_base()
Base.metadata.create_all(bind=engine)


def init():
    return init_app()
