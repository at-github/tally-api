from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

import models.transactions.crud as crud
from models.transactions.crud import\
    SortTransactionsEnum, OrderTransactionsEnum
from models.database import get_db
from schemas.transaction_associations import\
    TransactionAssociations, TransactionCreate

router = APIRouter()


@router.get('/transactions', status_code=status.HTTP_200_OK)
def get_transactions(
    db: Session = Depends(get_db),
    sort: SortTransactionsEnum | None = SortTransactionsEnum.DATE,
    order: OrderTransactionsEnum | None = OrderTransactionsEnum.DESC
) -> list[TransactionAssociations]:

    return crud.get_transactions(db, sort, order)


@router.get('/transactions/{id}', status_code=status.HTTP_200_OK)
def get_transaction(
    id: int,
    db: Session = Depends(get_db)
) -> TransactionAssociations:
    db_transaction = crud.get_transaction(db, transaction_id=id)

    if db_transaction is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Transaction not found"
        )

    return db_transaction


@router.post('/transactions', status_code=status.HTTP_201_CREATED)
def post_transaction(
        transaction: TransactionCreate,
        db: Session = Depends(get_db)
) -> TransactionAssociations:
    return crud.create_transaction(db=db, transaction=transaction)


@router.put('/transactions/{id}', status_code=status.HTTP_200_OK)
def put_transaction(
    id: int,
    transaction: TransactionCreate,
    db: Session = Depends(get_db)
) -> TransactionAssociations:
    db_transaction = crud.get_transaction(db, transaction_id=id)

    if db_transaction is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Transaction not found"
        )

    return crud.update_transaction(
        db=db,
        transaction_id=id,
        transaction=transaction
    )


@router.delete('/transactions/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_transaction(id: int, db: Session = Depends(get_db)) -> None:
    db_transaction = crud.get_transaction(db, transaction_id=id)

    if db_transaction is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Transaction not found"
        )

    crud.delete_transaction(db, transaction_id=id)
