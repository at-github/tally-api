from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from models.database import get_db
import models.third_parties.crud as crud
from schemas.third_party import ThirdParty, ThirdPartyCreate
from schemas.third_party_associations import ThirdPartyAssociations

router = APIRouter()


@router.get('/third-parties', status_code=status.HTTP_200_OK)
async def get_third_parties(
    db: Session = Depends(get_db),
) -> list[ThirdPartyAssociations]:
    return crud.get_third_parties(db)


@router.get('/third-parties/{id}', status_code=status.HTTP_200_OK)
def get_third_party(
    id: int,
    db: Session = Depends(get_db)
) -> ThirdPartyAssociations:
    db_third_party = crud.get_third_party(db, third_party_id=id)

    if db_third_party is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Third party not found"
        )

    return db_third_party


@router.post('/third-parties', status_code=status.HTTP_201_CREATED)
def post_third_party(
        third_party: ThirdPartyCreate,
        db: Session = Depends(get_db)
) -> ThirdParty:
    return crud.create_third_party(db=db, third_party=third_party)


@router.put('/third-parties/{id}', status_code=status.HTTP_200_OK)
def put_transaction(
    id: int,
    third_party: ThirdPartyCreate,
    db: Session = Depends(get_db)
) -> ThirdParty:
    db_third_party = crud.get_third_party(db, third_party_id=id)

    if db_third_party is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Third party not found"
        )

    return crud.update_third_party(
        db=db,
        third_party_id=id,
        third_party=third_party
    )


@router.delete('/third-parties/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_third_party(id: int, db: Session = Depends(get_db)) -> None:
    db_third_party = crud.get_third_party(db, third_party_id=id)

    if db_third_party is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Third party not found"
        )

    crud.delete_third_party(db, third_party_id=id)
