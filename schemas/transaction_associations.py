from typing import Optional

from .transaction import Transaction, TransactionBase
from .third_party import ThirdParty


class TransactionAssociations(Transaction):
    third_party: Optional[ThirdParty] = None


class TransactionCreate(TransactionBase):
    third_party_id: int
