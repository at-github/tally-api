from typing import Optional

from .third_party import ThirdParty
from .transaction import Transaction


class ThirdPartyAssociations(ThirdParty):
    transactions: Optional[list[Transaction]] = None
