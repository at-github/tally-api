from pydantic import BaseModel


class ThirdPartyBase(BaseModel):
    name: str


class ThirdPartyCreate(ThirdPartyBase):
    pass


class ThirdParty(ThirdPartyBase):
    id: int

    class ConfigDict:
        from_attributes = True
