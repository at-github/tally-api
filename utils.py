import os


def get_env_extension():
    env = os.environ.get('ENV')

    if env == 'test':
        return '.env.test'

    if env == 'prod':
        return ".env"

    return '.env.dev'
