"""Create third_parties

Revision ID: 866f05c52bcb
Revises: 7c1d1aec9134
Create Date: 2024-01-29 19:27:18.765413

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '866f05c52bcb'
down_revision: Union[str, None] = '7c1d1aec9134'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('third_parties',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_third_parties_id'), 'third_parties', ['id'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_third_parties_id'), table_name='third_parties')
    op.drop_table('third_parties')
    # ### end Alembic commands ###
